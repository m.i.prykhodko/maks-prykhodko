"use strict";

// task 1
//  Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let pFinder = document.querySelectorAll("p")
pFinder.forEach((p) => {
    p.style.background = "#ff0000";
})
console.log(pFinder);

// task 2
// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let listIdOpt = document.getElementById("optionsList");
console.log(listIdOpt);
console.log(listIdOpt.parentNode);
if (listIdOpt.hasChildNodes()) {
    let children = listIdOpt.childNodes;
    for (let i = 0;
        i < children.length;
        ++i) {
        let child = children[1];
        console.log(child.nodeName, child.nodeType);
    }
};

// task 3

// Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph


let testParElement = document.getElementById("testParagraph");
if (testParElement) {
    testParElement.textContent = "This is a paragraph";
}
console.log(testParElement);

// tasks 4 

//Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

let mainHeader = document.querySelector(".main-header");
let nestedEl = mainHeader.querySelectorAll("*");

nestedEl.forEach((element) => {
    element.classList.add("nav-item");
    console.log(element);
});


// task 5

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let titleEl = document.querySelectorAll(".section-title");

titleEl.forEach((element) => {
    element.classList.remove("section-title");
});



