"use strict";

let userNumber;
do {
    userNumber = parseInt(+prompt("Enter the number"));
}
while (
    isNaN(userNumber) || !Number.isInteger(userNumber)
);

let message = "";
let hasNumbers = false;
for (let i = 0; i <= userNumber; i++) {
    if (i % 5 === 0) {
        message += i + " ";
        hasNumbers = true;
    }
}
if (hasNumbers) {
    console.log(message);
} else {
    console.log("Sorry, no numbers");
}


let m, n;
do {
    m = parseInt(prompt("Введіть перше ціле число:"));
    n = parseInt(prompt("Введіть друге ціле число:"));
} while (isNaN(m) || isNaN(n) || !Number.isInteger(m) || !Number.isInteger(n));

let primeNumbersMessage = "\n";
let hasPrimeNumbers = false;
for (let i = Math.min(m, n); i <= Math.max(m, n); i++) {
    if (isPrime(i)) {
        primeNumbersMessage += i + " ";
        hasPrimeNumbers = true;
    }
}

function isPrime(num) {
    if (num < 2) {
        return false;
    }
    for (let i = 2; i <= Math.sqrt(num); i++) {
        if (num % i === 0) {
            return false;
        }
    }
    return true;
}

if (hasPrimeNumbers) {
    console.log(primeNumbersMessage);
} else {
    console.log("Sorry, no prime numbers");
}
