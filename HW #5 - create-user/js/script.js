"use strict";

// Технічні вимоги:
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі(наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser().Викликати у цього юзера функцію getLogin().Вивести у консоль результат виконання функції.
//     Необов'язкове завдання підвищеної складності
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму.Створити функції - сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.



function createNewUser() {

    const newUser = {};

    // при другому завданні

    // let firstName = '';
    // let lastName = '';

    // Object.defineProperty(newUser, 'firstName', {
    //     get: function () {
    //         return firstName;
    //     },
    //     set: function (value) {
    //         if (typeof value === 'string') {
    //             firstName = value;
    //         }
    //     },
    //     enumerable: true,
    //     configurable: true
    // });

    // Object.defineProperty(newUser, 'lastName', {
    //     get: function () {
    //         return lastName;
    //     },
    //     set: function (value) {
    //         if (typeof value === 'string') {
    //             lastName = value;
    //         }
    //     },
    //     enumerable: true,
    //     configurable: true
    // });

    // newUser.setFirstName = function (value) {
    //     this.firstName = value;
    // };

    // newUser.setLastName = function (value) {
    //     this.lastName = value;
    // };

    // newUser.setFirstName(prompt("Enter your name"));
    // newUser.setLastName(prompt("Enter your last name"));


    // при першому варіанті
    newUser.firstName = prompt("Enter your name");
    newUser.lastName = prompt("Enter your last name");

    newUser.getLogin = function () {
        return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    };
    return newUser;
}

const user = createNewUser();
console.log(user.getLogin());