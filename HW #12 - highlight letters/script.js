"use strict";

const buttons = document.querySelectorAll('.btn');
let currentKey = null;

function updateButtonState(key) {
    buttons.forEach(button => {
        if (button.textContent.toUpperCase() === key) {
            button.style.backgroundColor = 'blue';
            button.classList.add('focused');
        } else {
            button.style.backgroundColor = 'black';
            button.classList.remove('focused');
        }
    });

    currentKey = key;
}

document.addEventListener('keydown', (event) => {
    const key = event.key.toUpperCase();
    let enterButton = null;

    buttons.forEach(button => {
        if (button.textContent.toUpperCase() === 'ENTER') {
            enterButton = button;
        }
    });

    if (enterButton && key === 'ENTER') {
        if (!enterButton.classList.contains('focused')) {
            enterButton.style.backgroundColor = 'blue';
            buttons.forEach(button => button.classList.remove('focused'));
            enterButton.classList.add('focused');
        } else {
            enterButton.style.backgroundColor = 'black';
            enterButton.classList.remove('focused');
            currentKey = key;
        }
    } else {
        updateButtonState(key);
    }
});

buttons.forEach((button) => {
    button.addEventListener('transitionend', () => {
        if (button.textContent === currentKey) {
            return;
        }
        button.classList.remove('focused');
        button.style.backgroundColor = 'black';
    });
});







// document.addEventListener('keydown', (event) => {
//     const key = event.key.toUpperCase();

//     let button = null;

//     for (let i = 0; i < buttons.length; i++) {
//         if (buttons[i].textContent === key) {
//             button = buttons[i];
//             break;
//         }
//     }
//     if (!button) {
//         return;
//     }

//     buttons.forEach(btn => {
//         btn.classList.remove('focused');
//         btn.style.backgroundColor = 'black';
//     });

//     button.style.backgroundColor = "blue";
//     button.classList.add('focused');

//     currentKey = key;

// });

// buttons.forEach((button) => {
//     button.addEventListener('transitionend', () => {
//         if (button.textContent === currentKey) {
//             return;
//         }
//         button.classList.remove('focused');
//         button.style.backgroundColor = 'black';
//     });
// });