"use strict";

// task 1

// Реалізувати функцію, яка отримуватиме масив елементів і виводитиме їх на сторінку у вигляді списку. 

// Технічні вимоги:
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.)
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

// Приклади масивів, які можна виводити на екран:
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];
// Можна взяти будь-який інший масив.


function arrayOutput(array, parent = document.body) {
    const arrayList = document.createElement('ul');
    parent.appendChild(arrayList);

    array.map((item) => {
        const arrayListItem = document.createElement('li');
        arrayListItem.textContent = item;
        arrayList.appendChild(arrayListItem);
    });
}

const arrayOne = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arrayTwo = ["1", "2", "3", "sea", "user", 23];

arrayOutput(arrayOne);
arrayOutput(arrayTwo);


// additional task1

// Необов'язкове завдання підвищеної складності:
// Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список. 

// Приклад такого масиву:
// ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
// Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.


function arrayOutputCheck(array, parent = document.body) {
    const arrayList = document.createElement('ul');
    parent.appendChild(arrayList);

    array.map((item) => {
        const arrayListItem = document.createElement('li');
        arrayList.appendChild(arrayListItem);

        if (Array.isArray(item)) {
            arrayOutputCheck(item, arrayListItem);
        } else {
            arrayListItem.textContent = item;
        }
    });
}

const arrayThree = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
arrayOutputCheck(arrayThree);

// Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.

let seconds = 3;
const timer = setInterval(() => {
    console.log(seconds);
    seconds--;

    if (seconds === 0) {
        clearInterval(timer);
        document.body.innerHTML = '';
        console.log('Page cleared!');
    }
}, 1000);