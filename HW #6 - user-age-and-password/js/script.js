"use strict";

// Технічні вимоги:
// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.

function createNewUser() {

    const newUser = {};

    newUser.firstName = prompt("Enter your name");
    newUser.lastName = prompt("Enter your last name");
    newUser.birthDate = dateConversion(prompt("Enter your birth date (dd.mm.yyyy)"));

    newUser.getLogin = function () {
        return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    };

    newUser.getAge = function () {
        const currentDate = new Date;
        const birthdayDate = new Date(this.birthDate);
        const age = currentDate.getFullYear() - birthdayDate.getFullYear();
        const monthDiff = currentDate.getMonth() - birthdayDate.getMonth();
        if (monthDiff < 0 || (monthDiff === 0 && currentDate.getDate() < birthdayDate.getDate())) {
            return age - 1;
        }
        return age;
    };

    newUser.getPassword = function () {
        const firstLetter = this.firstName.charAt(0).toUpperCase();
        const lastNameLower = this.lastName.toLowerCase();
        const birthYear = this.birthDate.split("/")[2];
        return firstLetter + lastNameLower + birthYear;
    };
    return newUser;
};

function dateConversion(dateString) {
    const dateDetails = dateString.split(".");
    const formattedDate = `${dateDetails[1]}/${dateDetails[0]}/${dateDetails[2]}`;
    return formattedDate;
}

const user = createNewUser();
console.log("User: ", user);
console.log("Age: ", user.getAge());
console.log("Login: ", user.getLogin());
console.log("Password: ", user.getPassword());




