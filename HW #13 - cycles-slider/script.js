"use strict";

const timeLeft = document.createElement('div');
timeLeft.className = 'timer';
timeLeft.innerHTML = "Залишилося: <span id='countdown'>3</span> сек";

const startBtn = document.createElement('button');
startBtn.id = 'startButton';
startBtn.textContent = "Почати";

const stopBtn = document.createElement('button');
stopBtn.id = 'stopButton';
stopBtn.textContent = "Припинити";

const resumeBtn = document.createElement('button');
resumeBtn.id = 'resumeButton';
resumeBtn.textContent = "Відновити показ";

document.body.appendChild(timeLeft);
document.body.appendChild(startBtn);
document.body.appendChild(stopBtn);
document.body.appendChild(resumeBtn);

const styleElement = document.querySelector('style');

styleElement.textContent += `
        .image-to-show {
            opacity: 0;
            transition: opacity 0.5s ease-in-out;
        }

        .show {
            opacity: 1;
        }
    `;


const images = document.querySelectorAll('.image-to-show');
const countdownEl = document.getElementById('countdown');
const startButton = document.getElementById('startButton');
const stopButton = document.getElementById('stopButton');
const resumeButton = document.getElementById('resumeButton');

let currentImageIndex = 0;
let countdown = 3;
let intervalId;

function showImage(index) {
    images.forEach((image, i) => {
        if (i === index) {
            image.classList.add('show');
        } else {
            image.classList.remove('show');
        }
    });
}

function startSlideshow() {
    startButton.disabled = true;
    intervalId = setInterval(() => {
        countdown--;
        countdownEl.textContent = countdown;
        if (countdown <= 0) {
            countdown = 3;
            currentImageIndex = (currentImageIndex + 1) % images.length;
            showImage(currentImageIndex);
        }
    }, 1000);
}

function stopSlideshow() {
    clearInterval(intervalId);
    startButton.disabled = false;
}

function resumeSlideshow() {
    startSlideshow();
}

startButton.addEventListener('click', startSlideshow);
stopButton.addEventListener('click', stopSlideshow);
resumeButton.addEventListener('click', resumeSlideshow);

showImage(currentImageIndex);
