// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// Необов'язкове завдання підвищеної складності
// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.

const filmsContainer = document.createElement('div');
filmsContainer.id = 'films-container';
document.body.appendChild(filmsContainer);

async function sendRequest(url, method = "GET", config) {
    const request = await fetch(url, { method: method, ...config });
    const result = request.json();
    return result;
}

function displayFilms(films) {
    films.sort((a, b) => a.episodeId - b.episodeId);

    films.forEach(film => {
        const filmList = document.createElement('div');
        filmList.classList.add('film-list');

        const filmTitle = document.createElement("h2");
        filmTitle.textContent = `Episode ${film.episodeId}: ${film.name}`;

        const filmSummary = document.createElement("p");
        filmSummary.innerHTML = `Summary:<br> ${film.openingCrawl}`;

        const charactersTitle = document.createElement("li");
        charactersTitle.classList.add('characters-title');
        charactersTitle.textContent = "Characters: ";

        const charactersList = document.createElement('ul');
        charactersList.classList.add('characters-list');

        filmList.appendChild(filmTitle);
        filmList.appendChild(filmSummary);
        filmList.appendChild(charactersTitle);
        filmList.appendChild(charactersList);

        filmsContainer.appendChild(filmList);

        loadCharacters(film.characters, charactersList);
    });
}

function loadCharacters(charactersURLs, charactersList) {
    charactersList.innerHTML = "";

    const loadAnimation = document.createElement('div');
    loadAnimation.classList.add('load-animation');
    loadAnimation.textContent = 'Loading...';
    loadAnimation.style.display = 'block';
    charactersList.parentNode.insertBefore(loadAnimation, charactersList.nextSibling);

    Promise.all(charactersURLs.map(url => sendRequest(url)))
        .then(characters => {
            characters.forEach(character => {
                const characterItem = document.createElement("li");
                characterItem.textContent = character.name;
                charactersList.appendChild(characterItem);
            });
        })
        .finally(() => {
            loadAnimation.style.display = 'none';
            charactersList.style.display = 'block';
        });
}

sendRequest('https://ajax.test-danit.com/api/swapi/films')
    .then(films => displayFilms(films))
    .catch(error => alert("Error with fetching films", error));