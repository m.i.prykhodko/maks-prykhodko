Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

1. коли ми отримує відповідь від сервера чи іншого джерела (парсер) ізнеправильним синтаксисом

let userData = '{"name":"Jackie", "age":69/}';// лишній знак "/"
try {
    let parsedData = JSON.parse(userData);
} catch (error) {
    console.error("Помилка парсингу JSON: " + error.message);
}

2. коли дані вводить користувач, але вони не повні

try {
    let userInput = prompt("введіть суму прописом:");
    if (!isNaN(userInput)) {
        throw new Error("Введені дані не є текстовою сумою");
    }
} catch (error) {
    console.error("Помилка: " + error.message);
}