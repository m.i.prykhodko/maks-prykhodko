// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// - cтворити просту HTML-сторінку з кнопкою Знайти по IP.
// - натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// - дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// - під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// - всі запити на сервер необхідно виконати за допомогою async await.
// Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.


const APIIP = "https://api.ipify.org/?format=json";
const API_IP_LOCATION = "http://ip-api.com/";
const blockRoot = document.querySelector("#root");
const btn = document.querySelector(".btn_ip");

async function sendRequest(url, method = "GET", config) {
    const req_response = await fetch(url, { method, ...config });
    return req_response;
};

async function getRequestIp() {
    const res = await sendRequest(APIIP);
    const { ip } = await res.json();
    const responceIP = await sendRequest(`${API_IP_LOCATION}json/${ip}`);
    const { timezone, country, city, regionName } = await responceIP.json();
    blockRoot.insertAdjacentHTML(
        "beforeend",
        `
      <div>Континент - ${timezone}</div>
      <div>Країна- ${country}</div>
      <div>Регіон- ${regionName}</div>
    <div>Місто - ${city}</div>
    `
    );
}

btn.addEventListener("click", (e) => {
    if (e.detail === 1) {
        getRequestIp();
    } else {
        e.preventDefault();
    }
});
