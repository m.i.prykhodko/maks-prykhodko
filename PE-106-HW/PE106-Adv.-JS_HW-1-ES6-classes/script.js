// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.


class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }
    get age() {
        return this._age;
    }
    get salary() {
        return this._salary;
    }

    set name(newName) {
        this._name = newName;
    }
    set age(newAge) {
        this._age = newAge;
    }
    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get salary() {
        return super.salary * 3;
    }
    get lang() {
        return this._lang;
    }

    set lang(newLang) {
        this._lang = newLang;
    }
}

const prog1 = new Programmer("Mr. Pickles", 21, 40000, ["JavaScript", "Python"]);
const prog2 = new Programmer("Sponge Bob", 25, 60000, ["Java", "C++"]);
const prog3 = new Programmer("Joey Tribbiani", 56, 80000, ["Java", "PHP", "Ruby"]);


console.log("Programmer 1:", prog1);
console.log("Programmer 2:", prog2);
console.log("Programmer 3:", prog3);