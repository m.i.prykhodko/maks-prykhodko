'use stict';

const changeThemeButton = document.createElement('button');
changeThemeButton.textContent = 'Змінити тему';
changeThemeButton.id = 'change-theme-button';

document.body.appendChild(changeThemeButton);

let currentTheme = localStorage.getItem('theme') || 'light-theme';
let currentBackgroundColor = localStorage.getItem('сolor') || 'white';

document.body.className = currentTheme;
document.body.style.backgroundColor = currentTheme === 'dark-theme' ? 'black' : 'white';


changeThemeButton.addEventListener('click', () => {
    const newTheme = currentTheme === 'light-theme' ? 'dark-theme' : 'light-theme';
    localStorage.setItem('theme', newTheme);
    document.body.className = newTheme;

    document.body.style.backgroundColor = newTheme === 'dark-theme' ? 'black' : 'white';

    currentTheme = newTheme;
});

