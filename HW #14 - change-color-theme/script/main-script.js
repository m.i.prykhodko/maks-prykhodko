"use strict";

const inputPassword = document.getElementById("input-password");
const passwordConfirm = document.getElementById("password-confirm");
const showPass = document.getElementById("show-pass");
const showPassConfirm = document.getElementById("show-pass-confirm");

showPass.addEventListener("click", () => {
    changePassVisibility(inputPassword, showPass);
});

showPassConfirm.addEventListener("click", () => {
    changePassVisibility(passwordConfirm, showPassConfirm);
});

function changePassVisibility(inputField, eyeIcon) {
    if (inputField.type === "password") {
        inputField.type = "text";
        eyeIcon.classList.remove("fa-eye");
        eyeIcon.classList.add("fa-eye-slash");
    } else {
        inputField.type = "password";
        eyeIcon.classList.remove("fa-eye-slash");
        eyeIcon.classList.add("fa-eye");
    }
}

document.querySelector(".password-form").addEventListener("submit", function (event) {
    event.preventDefault();
    if (inputPassword.value === passwordConfirm.value) {
        alert("You are welcome");
    } else {
        const errorText = document.createElement("p");
        errorText.textContent = "Потрібно ввести однакові значення";
        errorText.style.color = "red";
        this.appendChild(errorText);
        setTimeout(function () {
            errorText.remove()
        }, 2000);
    }
});