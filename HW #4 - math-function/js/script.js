"use strict";

function mathOperation(firstUserNumber, secondUserNumber, userMathFunction) {
    if (userMathFunction === "+") {
        return firstUserNumber + secondUserNumber;
    } else if (userMathFunction === "-") {
        return firstUserNumber - secondUserNumber;
    } else if (userMathFunction === "*") {
        return firstUserNumber * secondUserNumber;
    } else if (userMathFunction === "/") {
        return firstUserNumber / secondUserNumber;
    } else {
        return NaN;
    }
}

let firstUserNumber, secondUserNumber, userMathFunction;

do {
    firstUserNumber = +prompt("Введіть перше число:");
    secondUserNumber = +prompt("Введіть друге число:");
} while (isNaN(firstUserNumber) || isNaN(secondUserNumber));

do {
    userMathFunction = prompt("Введіть математичну операцію (+, -, *, /):");
} while (userMathFunction !== "+" && userMathFunction !== "-" && userMathFunction !== "*" && userMathFunction !== "/");

const result = mathOperation(firstUserNumber, secondUserNumber, userMathFunction);

if (isNaN(result)) {
    console.log("Некоректна операція!");
} else {
    console.log(result);
}