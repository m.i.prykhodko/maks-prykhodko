"use strict";

let tabsTitle = document.querySelectorAll(".tabs-title");
let tabsContent = document.querySelectorAll(".tabs-content li");

function showContent(tabValue) {
    tabsContent.forEach((content) => {
        content.style.display = 'none';
    });

    tabsTitle.forEach((tab) => {
        tab.classList.remove('active');
    });

    let index = Array.from(tabsTitle).indexOf(tabValue);
    tabValue.classList.add('active');
    tabsContent[index].style.display = 'block';
}
tabsContent.forEach((content, index) => {
    if (index !== 0) {
        content.style.display = 'none';
    }
});

tabsTitle.forEach((tabValue) => {
    tabValue.addEventListener('click', () => {
        showContent(tabValue);
    });
});