"use strict";

function fibonacci(n) {
    if (n >= 0) {
        let F0 = 1;
        let F1 = 1;
        let F2
        for (let i = 1; i <= n; i++) {
            F2 = F0 + F1;
            F0 = F1;
            F1 = F2;
        }
        return F2;
    } else {
        let Fmin1 = 1;
        let Fmin2 = 1;
        let Fmin3
        for (let i = -1; i >= n; i--) {
            Fmin3 = Fmin1 - Fmin2;
            Fmin1 = Fmin2;
            Fmin2 = Fmin3;
        }
        return Fmin3;
    }
}
console.log(fibonacci(+prompt("Введіть число")))