"use strict";

let userNumber;

do {
    userNumber = +(prompt("Введіть число"));
} while (
    userNumber === "" || userNumber === null || isNaN(userNumber));

function factorial(userNumber) {
    return (userNumber !== 1) ? userNumber * factorial(userNumber - 1) : 1;
}
console.log(factorial(userNumber));

